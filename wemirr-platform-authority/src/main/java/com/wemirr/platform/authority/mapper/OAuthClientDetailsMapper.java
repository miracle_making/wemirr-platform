package com.wemirr.platform.authority.mapper;

import com.wemirr.framework.boot.SuperMapper;
import com.wemirr.platform.authority.domain.entity.OAuthClientDetails;
import org.springframework.stereotype.Repository;

/**
 * @author Levin
 */
@Repository
public interface OAuthClientDetailsMapper extends SuperMapper<OAuthClientDetails> {


}
